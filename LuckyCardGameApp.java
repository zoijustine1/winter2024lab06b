import java.util.Scanner;
public class LuckyCardGameApp {
	public static void main(String[] args){
		runGame();
	}
	
	/*This method returns nothing and takes no parameters. It creates a GameManager object
	* and goes throught a loop. the loop ends when the number of cards is less than or equal to 0 or 
	* when the total points is greater than 5. If totalpoints is less than 5, the player loses.
	* Otherwise, the player wins.
	*/
	public static void runGame(){
		Scanner reader = new Scanner(System.in);
		GameManager manager = new GameManager();
		int totalPoints = 0;
		System.out.println("Welcome to Lucky! Card game!");
		int i = 1;
		final int pointsNeeded = 5;
		while(manager.getNumberOfCards() > 1 && totalPoints < pointsNeeded){
			System.out.println("Round: " + i);
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			System.out.println("Your points after round " + i + ": " +totalPoints);
			manager.dealCards();
			i++;
		}
		if(totalPoints < pointsNeeded){
			System.out.println("Player loses with: " + totalPoints + " points");
		}
		else {
			System.out.println("Player wins with: " + totalPoints + " points");
		}
	}
}