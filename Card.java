public class Card {
	private String suit;
	private String value;
	
	//Create a constructor
	public Card(String value, String suit){
		this.suit = suit;
		this.value = value;
	}
	
	//Create get methods
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	
	//create toString method
	public String toString(){
		return this.value + " of " + this.suit;
	}
}