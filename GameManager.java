public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	/*
	* Create a constructor that shuffles the deck using the Deck class and initializes
	* the centerCard and playerCard fields to draw the top card from the deck using the Deck class
	*/
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	/*Create a toString to return a string containing the cards of the centerCard and player card.*/
	public String toString(){
		return "Center card: " + this.centerCard+"\nPlayer card: " + this.playerCard;
	}
	
	/*This method allows the center card and player card to draw a card from the deck.*/
	public void dealCards(){
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	/*This method returns the number of cards left in the deck.*/
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	
	/* This method returns an integer representing the points of the player. If the two cards have the same
	* value or number, the player gets 4 points. If the suit is the same, the player gets 2 points and if 
	* the card doesn't match the suit or the value, 1 point is deducted from the player
	*/
	public int calculatePoints(){
		if(this.centerCard.getValue() == this.playerCard.getValue()){
			return 4;
		}
		else if(this.centerCard.getSuit() == this.playerCard.getSuit()){
			return 2;
		}
		else {
			return -1;
		}
	}
}